import React from "react";
import { connect } from "react-redux";
import Task from './Task';
import RemoveBtn from './RemoveBtn';

const mapStateToProps = state => {
  return { tasks: state.tasks };
};

const ConnectedList = ({ tasks }) => {
  let emptyMessage = !tasks.length ? <i> No tasks on board </i> : '';
  let removeBtn = tasks.length > 1 ? <RemoveBtn /> : '';
  return (
    <>
      <br />
      <p> {emptyMessage} </p>
      <ul className="list-group list-group-flush">
        {tasks.map(el => (
          <>
            <li className="list-group-item" key={el.id}>
              {el.title}
              <Task el={el}/>
            </li>
          </>
        ))}
      </ul>
      <br />
      {removeBtn}
    </>
  );
}

const List = connect(mapStateToProps)(ConnectedList);

export default List;
