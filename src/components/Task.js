import React, { Component } from "react";
import { connect } from "react-redux";
import { removeTask } from "../redux/actions";

import EditTask from './EditTask';

const mapDispatchToProps = (dispatch) => ({
    removeTask: task => dispatch(removeTask(task))
});

class ConnectedTask extends Component {
  render() {
    return (
      <>
        <button type="button" className="close" aria-label="Close" onClick={() => this.props.removeTask(this.props.el)}>
          <span aria-hidden="true">&times;</span>
        </button>
        <EditTask el={this.props.el}/>
      </>
    );
  }
}

const Task = connect(null, mapDispatchToProps)(ConnectedTask);

export default Task;
