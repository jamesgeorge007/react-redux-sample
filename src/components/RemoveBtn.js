import React, { Component } from 'react';
import { connect } from "react-redux";
import { removeAllTasks } from '../redux/actions';

const mapDispatchToProps = (dispatch) => ({
    removeAllTasks: task => dispatch(removeAllTasks(task))
});

class ConnectedBtn extends Component {
  render() {
    return (
      <button className="btn btn-danger btn-md" onClick={() => this.props.removeAllTasks()}>
        Remove All
      </button>
    );
  }
};

const RemoveBtn = connect(null, mapDispatchToProps)(ConnectedBtn);

export default RemoveBtn;
