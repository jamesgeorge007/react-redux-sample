import React, { Component } from "react";
import { connect } from "react-redux";
import { editTask } from "../redux/actions";

const mapDispatchToProps = (dispatch) => ({
    editTask: task => dispatch(editTask(task))
});

class ConnectedTask extends Component {
  handleClick() {
    let updatedTask = prompt("Enter the updated task");
    if (!updatedTask) {
      return;
    }
    let task = this.props.el;
    this.props.editTask({task, updatedTask});
  }

  render() {
    return (
      <button type="button" className="close" aria-label="Close" onClick={() => this.handleClick()}>
        <span aria-hidden="true">&rarr;</span>
      </button>
    );
  }
}

const EditTask = connect(null, mapDispatchToProps)(ConnectedTask);

export default EditTask;
