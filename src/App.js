import React from "react";
import List from "./components/List";
import Form from "./components/Form";

const App = () => (
  <div className="row mt-5">
    <div className="col-md-4 offset-md-1">
      <h1 className="lead"><b>Add a new task</b></h1>
      <br />
      <Form />
    </div>
    <div className="col-md-4 offset-md-1">
      <h1 className="lead"><b>Tasks</b></h1>
      <List />
    </div>
  </div>
);

export default App;
