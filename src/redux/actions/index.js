const addTask = (payload) => {
  return {
    type: "ADD_TASK",
    payload
  }
};

const removeTask = (payload) => {
  return {
    type: "REMOVE_TASK",
    payload
  }
};

const removeAllTasks = () => {
  return {
    type: "REMOVE_ALL_TASKS"
  }
};

const editTask = (payload) => {
  return {
    type: "EDIT_TASK",
    payload
  }
};

export {
  addTask,
  removeTask,
  removeAllTasks,
  editTask
};
