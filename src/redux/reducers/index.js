import { Stats } from "fs";

const initialState = {
  tasks: []
};

const rootReducer = (state = initialState, action) => {
  let tasks;
  
  switch (action.type) {
    case "ADD_TASK":
      let el = state.tasks.find(item => item.title === action.payload.title)
      if (!el) {
        tasks = state.tasks.concat(action.payload);
        return {
          ...state,
          tasks
        };
      } else {
        alert("Task already preset within the list");
      }

    case "REMOVE_TASK":
      tasks = state.tasks.filter(item => item !== action.payload);
      return {
        ...state,
        tasks
      };

    case "REMOVE_ALL_TASKS":
      tasks = [];
      return {
        ...state,
        tasks
      }

    case "EDIT_TASK":
      tasks = [...state.tasks];
      let index = state.tasks.indexOf(action.payload.task);
      tasks[index].title = action.payload.updatedTask;
      return {
        ...state,
        tasks
      };

    default:
      return state;
}
return state;
};

export default rootReducer;
